import React, { Component} from 'react';
import './App.css';
import { Modal,Button,Container, Row, Col, InputGroup, FormControl, DropdownButton, Dropdown } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import DateTimePicker from 'react-datetime-picker';
import Countdown from 'react-countdown-now';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: new Date(),
      value: 'A',
      selectedValue: 'A',
      data:[]

    }

  }

  onChange = date => this.setState({ date })
  handleChange(event) {
    this.setState({ value: event.target.value })
  }
  

  
 

  handleSubmit = (evt) => {
    const selectedValue = evt
    this.setState({
      selectedValue: selectedValue
    })

    if(this.state.selectedValue === "A") {
      this.setState({
        data:["A","A1"]
      }, () => console.log(this.state.data))
    if(this.state.selectedValue === "B") {
      this.setState({
        data:["B","B1","B2"]
      }, () => console.log(this.state.data))
    }
    if(this.state.selectedValue === "C") {
      this.setState({
        data:["C"]
      }, () => console.log(this.state.data))
    }
    if(this.state.selectedValue === "D") {
      this.setState({
        data:["D","D1","D2","D3"]
      }, () => console.log(this.state.data))
    }
  }
}

handleShow = () => {
  const date = this.state.date
  const statemana = this.state.selectedValue
  
  const datamana = this.state.data
  alert(`State:` +statemana + `\nTime:` +date + `\nBranch:` + datamana)
}

  render() {
   
    return (
      <Container fluid>
        <Row className="bg-image">
          <Container>
            <Row>
              <Col>
                <div class="container-block-menu">
                  <ul class="menu-atas" >
                    <li class="logo"><img class="img-fluid" src="images/logo.png" alt="logo" width="300" /></li>
                    <li>Find A FurFriend</li>
                    <li>Adopt Guide</li>
                  </ul>
                </div>
                <div class="featured-notes">
                  <h2>Last Chance To Adopt</h2>
                  <Countdown date={Date.now() + 1000000000} />

                  <div class="button-red">Make Appointment Now</div>
                </div>
              </Col>
            </Row>
          </Container>
        </Row>
        <Container>
          <Row>
            <Col className="section2">
              <h2>Only heroes - adopt.</h2>
              <p><strong>Not all heroes wear capes!</strong> Saving a world is not a job that is only exclusive to comic heroes in new york city...</p>
              <p>Now you can do your part with one paw at a time.</p>
            </Col>
          </Row>
          <Row>
            <ul class='marker-list'>
              <Row>
                <Col lg={4} xs={12}>
                  <img class="img-fluid" src="images/graphic_01.png" alt="graphic1" />
                  <div class="info-marker">
                    <h5>True Companion</h5>
                    <p>Opening your door to your new furry friend bring a wholesome new meaning to friendship</p>
                  </div>
                </Col>
                <Col lg={4} xs={12}>
                  <img class="img-fluid" src="images/graphic_02.png" alt="graphic2" />
                  <div class="info-marker">
                    <h5>FullFilling Role</h5>
                    <p>The major side effect of being there for your furry friend is feeling very rewarded when they purr</p>
                  </div>
                </Col>
                <Col lg={4} xs={12}>
                  <img class="img-fluid" src="images/graphic_03.png" alt="graphic2" />
                  <div class="info-marker">
                    <h5>Loving Community</h5>
                    <p>You will be part of huge and growing community of adopters worldwide to share your experiences.</p>
                  </div>
                </Col>
              </Row>
            </ul>
          </Row>
        </Container>
        <Row className="hallmeow">
          <Container>
            <Row className="sidehalf">
              <Col>
                <h2>Hall of meows.</h2>
                <p>Featured felines of the week!</p>
                <Row className="linesofcat">
                  <Col>
                    <img class="catlist img-fluid" src="images/gallery_01.png" alt="gallery1" />
                  </Col>
                  <Col>
                    <img class="catlist img-fluid" src="images/gallery_02.png" alt="gallery1" />
                  </Col>
                  <Col>
                    <img class="catlist img-fluid" src="images/gallery_03.png" alt="gallery1" />
                  </Col>
                </Row>
                <Row className="linesofcat">
                  <Col>
                    <img class="catlist img-fluid" src="images/gallery_04.png" alt="gallery4" />
                  </Col>
                  <Col>
                    <img class="catlist img-fluid" src="images/gallery_05.png" alt="gallery5" />
                  </Col>
                  <Col>
                    <img class="catlist img-fluid" src="images/gallery_06.png" alt="gallery6" />
                  </Col>
                </Row>
                <div class="button-red browse">Browse More</div>
              </Col>
            </Row>
          </Container>
        </Row>
        <Row className="adopting">


          <Container>
            <Row className="adopt-side">
              <h2>Adopting 101</h2>
              <p><strong>Class is in session!</strong> Here's a basic guide of what you would probably want to have along with you at our adoption center:</p>
              <ul>
                <li>AN OPEN MIND <span class="subtle">Not all furry creatures are alike!</span></li>
                <li>COMFORTABLE SHOES <span class="subtle">You'll be on your feet all day, trust us!</span></li>
                <li>MOBILE PHONE <span class="subtle">To take pictures of your potential furry companion of course!</span></li>
              </ul>
            </Row>
            <Row>
              <div class="button-red letsadopt">Let's Adopt</div>
            </Row>
          </Container>
        </Row>

        <Row className="appointment">
          <div class="bg-place"><img src="images/cat1.png" alt="img-footer" /></div>
          <Container>

            <Row>
              <Col className="appointment-text">
                <h2>Make an Appointment</h2>
                <p>Time is gold! Make appointment to prevent dissapointment!</p>
              </Col>
            </Row>
            <Row>
              <Col lg={3} xs={12} className="date">
                <DateTimePicker
                  onChange={this.onChange}
                  value={this.state.date}
                />
              </Col>

              <Col lg={3} xs={12} className="dropdown">
                <DropdownButton onSelect={this.handleSubmit}
                  id="dropdown-item-button"
                  title="State">
                  <Dropdown.Item as="button" eventKey="A">State A</Dropdown.Item>
                  <Dropdown.Item as="button" eventKey="B">State B</Dropdown.Item>
                  <Dropdown.Item as="button" eventKey="C">State C</Dropdown.Item>
                  <Dropdown.Item as="button" eventKey="D">State D</Dropdown.Item>
                </DropdownButton>
              </Col>
              <Col lg={3} xs={12} className="dropdown">
                <DropdownButton id="dropdown-item-button" onSelect={this.handleSubmit} title="Branch Name">
                  {
                    this.state.selectedValue === 'A' ?
                      <>

                        <Dropdown.Item as="button" eventKey="Branch A">Branch A</Dropdown.Item>
                        <Dropdown.Item as="button" eventKey="Branch A1">Branch A1</Dropdown.Item>
                      </>
                      : null}
                  {
                    this.state.selectedValue === 'B' ?
                      <>

                        <Dropdown.Item as="button" eventKey="Branch B">Branch B</Dropdown.Item>
                        <Dropdown.Item as="button"eventKey="Branch B1">Branch B1</Dropdown.Item>
                        <Dropdown.Item as="button" eventKey="Branch B2">Branch B2</Dropdown.Item>
                        <Dropdown.Item as="button" eventKey="Branch B3">Branch B3</Dropdown.Item>
                      </>
                      : null}

                  {
                    this.state.selectedValue === 'C' ?
                      <>

                        <Dropdown.Item as="button" eventKey="Branch C">Branch C</Dropdown.Item>

                      </>
                      : null}


                  {
                    this.state.selectedValue === 'D' ?
                      <>

                        <Dropdown.Item as="button" eventKey="Branch D">Branch D</Dropdown.Item>
                        <Dropdown.Item as="button" eventKey="Branch D1">Branch D1</Dropdown.Item>
                        <Dropdown.Item as="button" eventKey="Branch D2">Branch D2</Dropdown.Item>

                      </>
                      : null}



                </DropdownButton>
              </Col>
                    <Button className="submit_btn" onClick={this.handleShow} lg={3} xs={12}>Submit</Button>
            </Row>
          </Container>
        </Row>
        <Row fluid className="extra">
          <Container>
            <Row>
              <Col xs={12} lg={6} className="volunteer">
                <h2>Extra hands?</h2>
                <p>YES please! Be a valunteer today:</p>
              </Col>
              <Col xs={12} lg={6} className="lockdown">
                <Row className="lockrow">
                  <Col lg={6} xs={12}>
                    <InputGroup className="mb-3">
                      <FormControl
                        placeholder="Username"
                        aria-label="Username"
                        aria-describedby="basic-addon1"
                      />
                    </InputGroup>
                  </Col>
                  <Col lg={6} xs={12}><div class="button-red helps">I WANT TO HELP</div>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col>
                <p class="copytext">(c) 2019 The Catastrophe</p>
              </Col>
              <Col className="priv-box">
                <ul class="privacy">
                  <li>FAQs</li>
                  <li>PRIVACY POLICY</li>
                  <li>LEGAL NOTICES</li>
                </ul>
              </Col>
            </Row>
          </Container>
        </Row>
        
      </Container>
    );
  }
}
